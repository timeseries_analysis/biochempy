# BioChemPy

A python package to fit parameters of equations, ODE or python functions that describe experimental data.


This code has been used in 

Thermodynamics and kinetics of protonated merocyanine photoacids in water

C Berton, DM Busiello, S Zamuner, E Solari, R Scopelliti, FF Tirani, et al.

Chemical Sciece, https://doi.org/10.1039/D0SC03152F