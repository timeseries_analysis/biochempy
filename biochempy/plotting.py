import matplotlib.pyplot as plt
import numpy as np

def plotModel(model,params=[],plots=[],width=20,height=20,pad=1.0,fontsize=20):
  colors=['black','crimson','blue','darkgreen','purple']
  figs_per_exp=[ len(plots[idx])*len(exp.replicates) for idx,exp in enumerate(model.experiments) ]
  ncols=min(2,max(figs_per_exp))
  nrows=int(np.sum(np.ceil([f/ncols for f in figs_per_exp])))
  print(ncols,nrows,len(figs_per_exp))
  fig, axs = plt.subplots(nrows=nrows, ncols=ncols)
  fig.set_figwidth(width)
  fig.set_figheight(height)
  cur_row=-1
  predictions=[ model.predict(p[0]) for p in params ]
  for eidx,exp in enumerate(model.experiments):
      cur_row+=1
      cur_col=-1
      for plot in plots[eidx]:
          for ridx,repl in enumerate(exp.replicates):
              cur_col+=1
              if ncols==1 and nrows==1:
                ax=axs
              elif ncols==1:
                ax=axs[ cur_row ]
              elif nrows==1:
                ax=axs[ cur_col ]
              else:
                  ax=axs[ cur_row, cur_col ]
              if plot[1] in repl.data.keys():
                ax.plot( repl.data[plot[0]].value,repl.data[plot[1]].value, label='experimental' )
                if repl.data[plot[1]].error is not None:
                    ax.fill_between(x=repl.data[plot[0]].value,y1=repl.data[plot[1]].value-repl.data[plot[1]].error,y2=repl.data[plot[1]].value+repl.data[plot[1]].error,color='grey',alpha=0.3)
                    ax.fill_between(x=repl.data[plot[0]].value,y1=repl.data[plot[1]].value-2*repl.data[plot[1]].error,y2=repl.data[plot[1]].value+2*repl.data[plot[1]].error,color='grey',alpha=0.3)
              for pidx,p in enumerate(predictions):
                  ax.plot( repl.data[plot[0]].value,p[eidx][ridx][plot[1]].value, label=params[pidx][1] )
              ax.set(xlabel=plot[0], ylabel=plot[1],title='{} - repl{}'.format(exp.name,ridx+1))
              ax.legend()
  fig.tight_layout(pad=pad)
  return fig

def plotPredictionErrors(model,v5,v16,v50,v84,v95,plots=[],width=20,height=20,pad=1.0,fontsize=20):
  figs_per_exp=[ len(plots[idx])*len(exp.replicates) for idx,exp in enumerate(model.experiments) ]
  ncols=min(2,max(figs_per_exp))
  nrows=int(np.sum(np.ceil([f/ncols for f in figs_per_exp])))

  fig, axs = plt.subplots(nrows=nrows, ncols=ncols)
  fig.set_figwidth(width)
  fig.set_figheight(height)
  cur_row=-1
  """
  predictions_mcmc=[[{k:[] for k in repl.data.keys()} for repl in exp.replicates] for exp in model.experiments]
  for param in params:
      p=model.predict(param)
      for iexp,exp in enumerate(p):
          for irepl,repl in enumerate(exp):
              for key in repl.keys():
                  try:
                    predictions_mcmc[iexp][irepl][key].append(repl[key].value)
                  except:
                    pass

  predictions_mcmc=[[{k:v for k,v in repl.items() if len(v)>0} for repl in exp] for exp in predictions_mcmc]

  for iexp,exp in enumerate(predictions_mcmc):
      for irepl,repl in enumerate(exp):
          for key in repl.keys():
              if len(predictions_mcmc[iexp][irepl][key])==0:
                  continue
              if key=='t':
                  predictions_mcmc[iexp][irepl][key]=predictions_mcmc[iexp][irepl][key][0]
              else:
                  predictions_mcmc[iexp][irepl][key]=np.quantile(np.stack(predictions_mcmc[iexp][irepl][key],axis=1),[0.05,0.16,0.50,0.84,0.95],axis=1)
  """
  for eidx,exp in enumerate(model.experiments):
      cur_row+=1
      cur_col=-1
      for plot in plots[eidx]:
          for ridx,repl in enumerate(exp.replicates):
              cur_col+=1
              if ncols==1:
                  ax=axs[ cur_row ]
              else:
                  ax=axs[ cur_row, cur_col ]
              ax.plot( repl.data[plot[0]].value,repl.data[plot[1]].value, label='experimental' )
              ax.fill_between(x=repl.data[plot[0]].value, y1=v5[eidx][ridx][plot[1]].value , y2=v95[eidx][ridx][plot[1]].value, color='grey',alpha=0.3)
              ax.fill_between(x=repl.data[plot[0]].value, y1=v16[eidx][ridx][plot[1]].value, y2=v84[eidx][ridx][plot[1]].value, color='grey',alpha=0.3)
              ax.plot(repl.data[plot[0]].value, v50[eidx][ridx][plot[1]].value, label='50% percentile', linewidth=2., color='black', linestyle=':')
#              ax.fill_between(x=repl.data[plot[0]].value, y1=predictions_mcmc[eidx][ridx][plot[1]][0],y2=predictions_mcmc[eidx][ridx][plot[1]][4],color='grey',alpha=0.3)
#              ax.fill_between(x=repl.data[plot[0]].value, y1=predictions_mcmc[eidx][ridx][plot[1]][1],y2=predictions_mcmc[eidx][ridx][plot[1]][3],color='grey',alpha=0.3)
#              ax.plot(repl.data[plot[0]].value,predictions_mcmc[eidx][ridx][plot[1]][2],label='50% percentile',linewidth=2., color='black',linestyle=':')
              ax.set(xlabel=plot[0], ylabel=plot[1],title='{} - repl{}'.format(exp.name,ridx+1))
              ax.legend()
  fig.tight_layout(pad=pad)
  return fig
