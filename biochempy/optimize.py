from biochempy.data import *
from copy import deepcopy
from lmfit import minimize, Parameters, conf_interval, Minimizer
import numpy as np
from tqdm import tqdm,tqdm_notebook

class Optimize():
  def __init__(self,model,indata,variables):
    self.initial_data=dict(indata)

    self.constants=dict(indata)
    self.initial_params=Parameters()
    self.optimal_params=Parameters()

    self.emcee_flat_samples=None

    for variable in indata:
      minv=indata[variable].value-3*indata[variable].error if indata[variable].error is not None else 0
      minv=indata[variable].minv if indata[variable].minv is not None else minv
      maxv=indata[variable].value+3*indata[variable].error if indata[variable].error is not None else None
      if variable in variables:
        self.initial_params.add(variable,value=indata[variable].value,min=minv,max=maxv,vary=True)
        del self.constants[variable]
      else:
        self.initial_params.add(variable,value=indata[variable].value,min=minv, max=maxv,vary=False)
    self.variables=[var for var in variables if var not in self.constants.keys()]
    self.model=model
  
  def loss(self,params,infinity=9999.):
    const=dict(self.constants)
    for p in self.variables:
      if self.initial_data[p].minv is not None and self.initial_data[p].maxv is not None:
        # data must be in specified range
        if np.any( np.less(params[p].value,self.initial_data[p].minv) ) or np.any( np.greater(params[p].value,self.initial_data[p].maxv) ):
          return infinity
      elif self.initial_data[p].minv is not None:
        # data must be bigger than threshold
        if np.any( np.less(params[p].value,self.initial_data[p].minv) ):
          return infinity
      elif self.initial_data[p].maxv is not None:
        # data must be smaller than threshold
        if np.any( np.greater(params[p].value,self.initial_data[p].maxv) ):
          return infinity
      const[p]=Data(params[p].value,self.initial_data[p].error)      
      
    l=self.model.loss(const)
    for v in self.variables:
      if self.initial_data[v].error is not None:
        l+=(const[v].value-self.initial_data[v].value)**2/self.initial_data[v].error**2
    if not np.isfinite(l):
      return infinity
    return l
  
  def listToParams(self,values):
    const=dict(self.constants)
    for idx,p in enumerate(self.variables):
      const[p]=Data(values[idx],self.initial_data[p].error,self.initial_data[p].minv,self.initial_data[p].maxv)
    return const
    
  def log_prob(self,params):
    const=self.listToParams(params)
    return -self.loss(const,infinity=np.inf)
    
  def minimize(self,method='nelder',maxiter=200,params=None,replicas=1,std=0.1,progressbar=True):
    if method=="nelder":
      fit_opt={'maxiter':maxiter,'adaptive':True}
    elif method=="lbfgsb":
      fit_opt={'maxiter':maxiter,'ftol': 1e-10, 'gtol': 1e-08, 'eps': 1e-10}
    else:
      fit_opt={}
    
    if params is None:
      params=self.initial_params
    for c in self.constants:
      params[c].set(vary=False)
    for v in self.variables:
      params[v].set(vary=True)
    
    if replicas==1:
      r= minimize(self.loss,params,method=method,options=fit_opt)
      self.optimal_params=deepcopy(r.params)
      return r
    elif replicas>1:
      minima=[minimize(self.loss,params,method=method,options=fit_opt)]
      for r in tqdm(range(replicas-1)):
        p=deepcopy(params)
        for k in self.variables:
          p[k].value=p[k].value+np.random.random()*p[k].value*std 
        minima.append(minimize(self.loss,p,method=method,options=fit_opt))
      l=np.inf
      idx=0
      for i,m in enumerate(minima):
        if m.residual<l:
          l=m.residual
          idx=i
      self.optimal_params=deepcopy(minima[idx].params)
      return minima[idx]
    else:
      raise RuntimeError("Number of replicas must be greater than zero")
                  
  def sampleMCMC(self,params=None,workers=1,burn=0,steps=200,thin=1,nwalkers=50,progress=True):
    import emcee
    if workers>1:
      import os
      os.environ["OMP_NUM_THREADS"] = "1"
      from pathos.multiprocessing import ProcessingPool as Pool
      pool = Pool(workers)
    else:
      pool=None
    
#    pos=np.zeros((nwalkers,result.nvarys))
    pos=np.zeros((nwalkers,len(self.variables)))
    
#    params=result.params
    if params is None:
      params=deepcopy(self.optimal_params)
    for idx,v in enumerate(self.variables):
      if self.initial_data[v].minv is not None and self.initial_data[v].maxv is not None:
        rnd=np.random.uniform(size=nwalkers,low=self.initial_data[v].minv-params[v].value,high=self.initial_data[v].maxv-params[v].value)
        pos[:,idx] = (params[v].value + rnd)
      elif self.initial_data[v].minv is not None and self.initial_data[v].maxv is None:
        maxx=self.initial_data[v].error if self.initial_data[v].error is not None else params[v].value*0.05
        rnd=np.random.uniform(size=nwalkers,low=self.initial_data[v].minv-params[v].value,high=maxx)
        pos[:,idx] = (params[v].value + rnd)
      elif self.initial_data[v].minv is None and self.initial_data[v].maxv is not None:
        minx=-self.initial_data[v].error if self.initial_data[v].error is not None else -params[v].value*0.05
        rnd=np.random.uniform(size=nwalkers,high=self.initial_data[v].maxv-params[v].value,low=minx)
        pos[:,idx] = (params[v].value + rnd)
      elif self.initial_data[v].error is not None:
        rnd=np.random.random((nwalkers))
        pos[:,idx] = (params[v].value + rnd*self.initial_data[v].error)
      else:
        rnd=np.random.random((nwalkers))
        pos[:,idx] = (params[v].value + rnd*params[v].value*0.05)
      
#    sampler = emcee.EnsembleSampler(nwalkers, result.nvarys,self.log_prob,pool=pool)
    sampler = emcee.EnsembleSampler(nwalkers, len(self.variables),self.log_prob,pool=pool)
    sampler.run_mcmc(pos, steps*thin+burn, progress=progress,skip_initial_state_check=True)
    self.emcee_flat_samples = sampler.get_chain(discard=burn, thin=thin, flat=True)
    
    try:
      tau=sampler.get_autocorr_time(c=1)
    except:
      tau=None
    return self.emcee_flat_samples,self.variables,[params[v].value for v in self.variables],tau
